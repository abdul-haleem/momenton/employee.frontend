var myapp = angular.module('myapp', ['ngSanitize']);
myapp.controller('main-controller', ['$scope', '$http', '$log', function(scope, http, log) {
    
    
    /*
    Method to get all employees
    */
    var getAllEmployees = function() {
        http.get("http://localhost:9090/employees")
        .then(function(result) {
            log.info("Getting all employees", result);
            scope.allEmployees = result.data;
        }, function(error) {
            log.error(error);
        });    
    }
    
    /*
    Get all employees and populate the table
    */
   getAllEmployees();
    
    
    
    /*
    Create new employee
    */
    scope.employeeManager = {};
    scope.employee = {};
    scope.createEmployee = function() {
        log.info(scope.employee);
        //Verify Id does not already exist
        for(var i=0; i<scope.allEmployees.length; i++) {
            if (scope.allEmployees[i].id == scope.employee.id) {
                alert("Id already exists");
                scope.employee.id = '';
                return;
            }
        }
        
        // Verify manager Id
        if (scope.allEmployees.length > 0 && (scope.employeeManager.id == undefined || scope.employeeManager.id == '')) {
            alert("Please select a manager");
            return;
        }
        scope.employee.managerId = scope.employeeManager.id;
        
        // Call service to create employee
        http.post("http://localhost:9090/employees", scope.employee)
        .then(function(result) {
            alert("Employee created");
            scope.updateScopeAfterAddingEmployee(result.data);
            log.info(result.data);
        }, function(error) {
            log.error(error);
            alert("Could not create employee");
        });
        
    }
    
    /*
    Method to update model after new employee is added
    */
    scope.updateScopeAfterAddingEmployee = function(data) {
        var employee = {};
        employee.id = data.id;
        employee.name = data.name;
        employee.managerId = data.managerId;
        scope.allEmployees.push(employee);
        scope.getEmployeeNoManager();
        scope.employeeManager = {};
        scope.employee = {};    
    }
    
    
    
    /*
    Call service and get employee hierarchy
    */
    scope.getEmployeeNoManager = function() {
        http.get("http://localhost:9090/employee-no-manager")
        .then(function(result) {
            scope.hierarchyData = '<table class="table table-bordered">';
            createHierarchyData(result.data);
            scope.hierarchyData += '</table>';
        }, function(error) {
            log.error(error);
        });
    }
    
    scope.getEmployeeNoManager();
    
    /*
    Method to create employee hierarchy table
    */
    var createHierarchyData = function (obj) {
        scope.hierarchyData += "<tr>"
        var lvl;
        if(obj.level > 1) {
            for (lvl=0; lvl<obj.level-1; lvl++) {
                scope.hierarchyData += "<td></td>"
            }
        }

        scope.hierarchyData += "<td>" + obj.name + "</td>";
        scope.hierarchyData += "</tr>"

        var myArr = obj.workers;
        var i;
        if (undefined !== myArr) {

            for (i=0; i<myArr.length; i++) {
                createHierarchyData(myArr[i]);
            }
        }
    }

 
    
}]);