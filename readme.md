Introduction
------------
This is an AngularJs application based on CDN links. It is not created using npm.

How to Run
----------
Just open index.html file in a browser. If backend application is running it will communicate with it and display data.